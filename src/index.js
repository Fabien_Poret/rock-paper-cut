import React from 'react';
import ReactDOM from 'react-dom';
import { RecoilRoot } from 'recoil'
import { Container } from './components'

import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <Container>
      <RecoilRoot>
        <App />
      </RecoilRoot>
    </Container>
  </React.StrictMode>,
  document.getElementById('root')
);