import { findIndex } from 'lodash'

export const whoWin = (actions, icon, random) => {
    console.log('icon', icon.iconName);

    const iconKey = findIndex(actions, function(o){return o.icon.iconName == icon.iconName})

    console.log('iconKey', iconKey);
    console.log('random', random)
    
    if(iconKey == random)
    {
        return 'Egalité'
    } else if(iconKey == 0 && random == 2){
        return 'Gagné'
    } else if(iconKey == 0 && random == 1){
        return 'Perdu'
    } else if(iconKey == 1 && random == 0){
        return 'Gagné'
    } else if(iconKey == 1 && random == 2){
        return 'Perdu'
    } else if(iconKey == 2 && random == 1){
        return 'Gagné'
    } else if(iconKey == 2 && random == 0){
        return 'Perdu'
    } else {
        return '?'
    }
}