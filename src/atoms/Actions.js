import { atom } from 'recoil'

export const actionsState = atom({
    key: 'actionsState',
    default: []
})