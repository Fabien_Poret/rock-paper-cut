import React, { useEffect } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { useRecoilState } from 'recoil'
import { faHandPaper, faHandRock, faHandScissors } from '@fortawesome/free-solid-svg-icons'

import Choice from './pages/Choice'
import Play from './pages/Play'

import { actionsState } from './atoms'

function App() {

  const [actions, setActions ] = useRecoilState(actionsState)

  useEffect(() => {
    setActions([
      {
        color: 'blue',
        icon: faHandPaper,
      },
      {
        color: 'green',
        icon: faHandScissors,
      },
      {
        color: 'red',
        icon: faHandRock,
      }
    ])
  }, [])

  return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Choice} />
          <Route exact path="/play" component={Play} />
        </Switch>
      </BrowserRouter>
  );
}

export default App;
