import React  from 'react'
import { useHistory  } from 'react-router-dom'
import { useRecoilState, useRecoilValue } from 'recoil'

import { Action, Score } from '../components'
import { actionsState, isSelectedState, scoreState} from '../atoms'

function Choice(){
    let history = useHistory()

    const [ selected, setSelected ] = useRecoilState(isSelectedState)

    const actionsValue = useRecoilValue(actionsState)
    const score =  useRecoilValue(scoreState)

    const handleActionClicked = (value) => {
        setSelected({
            ...value
        })

        history.push('/play')
    } 

    return(
        <>
            <h1>Pierre, feuille ou ciseaux ? </h1>
            <Score score={score}/>
            {actionsValue.map(action => {
                return <Action color={action.color} icon={action.icon} handleActionClicked={handleActionClicked}/>
            })}
        </>
    )
}

export default Choice