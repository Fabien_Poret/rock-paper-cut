import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ActionButton = styled.button`
    background-color: white;
    border: 1rem solid  ${props => props.borderColor || 'gold'};
    border-radius: 50%;
    color: #3B4262;
    cursor: pointer;
    height: 200px;
    margin: 1%;
    transform: rotate(45deg);
    width: 200px;
`

export function Action({color, icon, isDisabled = false, handleActionClicked}){
    return (
        <ActionButton borderColor={color} onClick={() => handleActionClicked(icon)} disabled={isDisabled}>
            <FontAwesomeIcon icon={icon} size="6x"/>
        </ActionButton>
    )
}
