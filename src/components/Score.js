import styled from 'styled-components'

const ScoreDiv = styled.div`

`

export function Score({score}){
    return(
        <ScoreDiv>
            Bien joué, votre score est de <b>{score}</b> victoire !
        </ScoreDiv>
    )
}